from setuptools import setup

setup(
    name='library-test',
    packages=['library-test'],
    description='Hello world library test',
    version='0.5',
    url='http://github.com/example/my_example',
    author='Fran',
    author_email='francisco@creditsesame.com',
    keywords=['pip','francisco','library-test']
    )